###v1.0.1
- added Filterable trait

###v1.0.3
- added support to filter between 

###v1.0.6
- added support to filterables

###v1.0.7
- added support to custom filters

###v1.0.9
- added support to extra data in ResourceController store/update

###v1.0.10
- fix support to extra data in ResourceController store/update
- added closure after save

###v1.0.11
- add support to disable pagination

###v1.0.12
- add support to multiple orderBy

###v1.0.14
- add new targets compatibility to laravel 6.*
- add Uploadable trait (alpha feature)

###v1.0.15
- with ``APP_ENV=production`` controller doesn't return exception message

###v1.0.16
- change exception control with ``APP_DEBUG=true``

###v1.0.18
- added SubQuery trait

###v1.0.19
- added Query Macros