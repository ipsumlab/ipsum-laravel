## Ipsumlab Laravel Utilities Package

What's included
* Responses
* Filterable Trait for Laravel Eloquent Models
* ResourceController

Remember to add in app.php
```
    'providers' => [ ...
        Ipsumlab\Core\Providers\ResponseMacroServiceProvider::class,
    ];
```

this will let you use 
`reponse()->datatable()` `reponse()->error()`  and `reponse()->success()`

#### ResourceController configuration

example
```
use Ipsumlab\Core\Http\Controllers\ResourceController;

class MyApiController extends ResourceController
{

protected $resource = MyModelResource::class;

protected $views = [
    'list' => 'slug.view.list', //blade format
    'create' => 'slug.view.list',
    'edit' => 'slug.view.list'
];

//used in store/update method
protected $validators = [
    'field1' => 'required',
    'field2' => 'required|numeric'
];

// used in index
protected $relations = ['relation'];

// used in destroy
protected $relations_delete = ['relation'];

...
}

```
#### New (store, update)
You can override normal save with this implementation

```
public function store(Request $request){
    ...

    $extra_data = ['somevar' => 'somevalue'];

    return parent::store($request, $extra_data, 
        function($resource) use ($request){
            //something to do after save
            upload_file();
            
            $resource->somecolumn = extra_operations($resource->id); 
            $resource->save();
        }
    );

}
```


#### Route configuration
```
    Route::resources([
        'your-api-name' => 'MyApiController',
    )]

    Route::get('/you-list-page', 'MyApiController@list');
```

#### Filterable Trait

example
```
class MyModel extends Model
{
    use Filterable;
    
    // Define these properties if you want to allow filters over certain fields
 
    // fields used in a orWhere condition with 'like' sql operator
    // values passed through $request->search

    protected static $searchable = ['title', 'subtitle', 'content'];
    
    protected static $relations_searchable = [
        'relation_1' => 'title',               
        'relation_2' => ['title', 'subtitle', 'content'],
    ];
    
    
    // values passed through $request->filters in key=>value format
    // you can specify what operator will be used or a custom function 
    // by default it use a standard ->where('field', 'value') condition

    protected static $filterable = [
        'standard_filter' => '>=' (or 'like', 'between', 'in')
        'custom_filter' => 'customFilterFunction'
    ];

    // By default if you do not define $filterable array 
    // ResourceController takes $request->filters and try to add all the present field in where condition

    ...

    public static function customFilterFunction($query, $value){
    
        return $query->whereHas('users', function($q) use ($value){
            $q->whereIn('user_id', $value);
        });
    }

```

#### Uploadable Trait
**Actually we use `Intervention\Image\ImageManagerStatic`` for image manipulation, install it if you want to use thumbnail functionality**  

There are some many options to customize

```
protected $files_directory = 'uploads';
protected $prefix = 'date';
protected $prefix_format = 'YmdHis';
protected $uglify_filename = false;
protected $id_directory = true;
protected $make_thumb = true;
```

Usage example: in your model put the code below  

```
class MyModel extends Model
{
    use Uploadable;
    
    // if you want to customize protected variables
    
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    
        $this->files_directory = 'app/public/media';
        $this->id_directory = false;
    }
    ...
}
```
Simple usage in a controller
```
 $myModel = MyModel::find(1);
 $myModel->upload($requet->file('myfile'), 'db_colum_where_store_filename', [$delete_old = false]);
```

This configuration put the files into  ``storage/app/public/media`` directory with no model related ``id`` directory for example ``storage/app/public/media/1/``

#### Subquery Trait

example usage
```
$myModel->selectSub(SomeModel::getSub('attribute_to_show', 'some_table.id'), 'some_alias_name');
```


#### Query builder Macros


Remember to add in app.php
```
    'providers' => [ ...
        Ipsumlab\Core\Providers\QueryMacroServiceProvider::class,
    ];
```

example usage
```
$myModel->searchIn($needle, 
    ['mymodel_attr1', 'mymodel_attr2'], 
    [
        'mymodel_rel1' => 'mymodel_rel1_attr1',               
        'mymodel_rel2' => ['mymodel_rel2_attr1', 'mymodel_rel2_attr2', 'mymodel_rel2_attr3'],
    ]);
```