<?php

namespace Ipsumlab\Core\Http\Controllers;

use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class ResourceController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * The resource model
     *
     * @var Model
     */
    protected $resource;

    /**
     * The views array used in create and edit
     *
     * @var array
     */
    protected $views = [
        'list' => '',
        'create' => '',
        'edit' => ''
    ];

    /**
     * The validators array
     *
     * @var array
     */
    protected $validators = [];

    /**
     * The validators array
     *
     * @var array
     */
    protected $validators_messages = [];

    /**
     * The eager load realationships array
     *
     * @var array
     */
    protected $relations = [];

    /**
     * The relationships to delete during destroy
     *
     * @var array
     */
    protected $relations_delete = [];

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param Builder $query
     * @return Response
     */
    public function index(Request $request, $query = null)
    {
        if(!empty($this->relations)){
            $results = $this->resource::filter($request->filters, $request->search['value'] ?? null, $query)
                ->with($this->relations);
        }elseif (method_exists($this->resource, 'filter')){

            $results = $this->resource::filter($request->filters, $request->search['value'] ?? null, $query);
        }else{
            $results = $this->resource::select('*');
        }

        $recordsTotal = $this->resource::count();
        $recordsFiltered = $results->count();

        $orderBy = $request->get('order');

        if(!empty($orderBy)){
            $columns = $request->get('columns');
            foreach ($orderBy as $order){
                $colIndex = $order['column'];
                $results->orderBy($columns[$colIndex]['data'], $order['dir']);
            }
        }

        if($request->get('length') && $request->get('length')>-1) {
            $results->limit($request->get('length'))
                    ->offset($request->get('start'));
        }

        $data = $results->get();

        return response()->datatable($data, $recordsTotal, $recordsFiltered, $request->get('draw'));
    }

    /**
     * Show the view list of resource
     *
     * @return View|Factory
     */
    public function list(){

        return view($this->views['list']);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return View|Factory
     */
    public function create()
    {
        return view($this->views['create'], ['id' => null]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        try{

            if(!empty($this->validators)){
                $validator = Validator::make($request->all(), $this->validators, $this->validators_messages);

                if ($validator->fails()) {
                    return response()->json(['errors' => $validator->getMessageBag()], 400);
                }
            }

            $this->resource = new $this->resource;

            $this->resource->fill($request->all());

            if(func_num_args() > 1) {
                $data = func_get_arg(1);
                if (!empty($data)) {
                    foreach ($data as $key => $value) {
                        $this->resource->$key = $value;
                    }
                }
            }

            if($this->resource->save()){

                if(func_num_args() > 2) {
                    $onSave = func_get_arg(2);
                    $onSave($this->resource);
                }
                return response()->json($this->resource);
            }

            return response()->json( ['errors' => 'Error while saving'],500);

        }catch (Exception $e){

            if(!env('APP_DEBUG', false)){
                return response()->json(['errors' => 'internal server error'], 500);
            }
            return response()->json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function show($id)
    {
        try{
            $this->resource = $this->resource::find($id);

            return response()->json($this->resource);

        }catch (Exception $e){

            if(!env('APP_DEBUG', false)){
                return response()->json(['errors' => 'internal server error'], 500);
            }
            return response()->json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return View|Factory
     */
    public function edit($id)
    {
        return view($this->views['edit'], ['id' => $id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int    $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        try{
            if(!empty($this->validators)){
                $validator = Validator::make($request->all(), $this->validators, $this->validators_messages);

                if ($validator->fails()) {
                    return response()->json(['errors' => $validator->getMessageBag()], 400);
                }
            }

            $this->resource = $this->resource::find($id);

            $this->resource->fill($request->all());

            if(func_num_args() > 2) {
                $data = func_get_arg(2);
                if (!empty($data)) {
                    foreach ($data as $key => $value) {
                        $this->resource->$key = $value;
                    }
                }
            }
            if($this->resource->save()){

                if(func_num_args() > 3) {
                    $onSave = func_get_arg(3);
                    $onSave($this->resource);
                }

                return response()->json($this->resource);
            }

            return response()->json( ['errors' => 'Error while saving'], 500);

        }catch (Exception $e){

            if(!env('APP_DEBUG', false)){
                return response()->json(['errors' => 'internal server error'], 500);
            }
            return response()->json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy($id)
    {
        try {
            $this->resource = $this->resource::find($id);

            foreach ($this->relations_delete as $relation) {
                $this->resource->{$relation}()->delete();

            }
            $this->resource->delete();

            return response()->json(['success' => true]);

        }catch (Exception $e){

            if(!env('APP_DEBUG', false)){
                return response()->json(['errors' => 'internal server error'], 500);
            }
            return response()->json(['errors' => $e->getMessage()], 500);
        }
    }
}
