<?php


namespace Ipsumlab\Core\Macros;

use Illuminate\Database\Eloquent\Builder;


class SearchIn
{
    /**
     * @param \Illuminate\Database\Query\Builder $builder
     * @var \PDO $pdo
     * @var array $entries
     * @var array $columnsArray
     * @var string $columnsString
     * @var string $valuesString
     * @var string $updateString
     * @var string $sql
     */
    protected $builder, $pdo;

    private $attributes;
    private $needle;
    private $relations;


    public function __construct(Builder $builder, $needle, $attributes, $relations = null)
    {
        $this->builder = $builder;
        $this->pdo = $this->builder->getConnection()->getPdo();

        $this->attributes = $attributes;
        $this->needle = $needle;
        $this->relations = $relations;
    }
    public function execute()
    {
        if(!empty($this->needle)){

            $this->builder->where(function($q){
                if(!empty($this->attributes)){
                    foreach ( $this->attributes as $attr) {
                        $q->orWhere($attr, 'like', "%" . $this->needle . "%");
                    }
                }

                if(!empty($this->relations)){
                    foreach ( $this->relations as $rel => $attr) {
                        $q->orWhereHas($rel, function ($q) use ($attr) {
                            if(is_array($attr)){
                                $q->where(function($q) use($attr){
                                    foreach ($attr as $a){
                                        $q->orWhere($a, 'like', "%" . $this->needle . "%");
                                    }
                                });
                            }else{
                                $q->where($attr, 'like', "%" . $this->needle . "%");
                            }
                        });
                    }
                }
            });
        }
        return $this->builder;
    }
}
