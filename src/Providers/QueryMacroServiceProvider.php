<?php

namespace Ipsumlab\Core\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Builder;
use Ipsumlab\Core\Macros\SearchIn;

class QueryMacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Builder::macro('searchIn', function($needle, $attributes, $relations ){
            return with(new SearchIn($this, $needle, $attributes, $relations))->execute();
        });
    }
}
