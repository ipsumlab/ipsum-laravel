<?php


namespace Ipsumlab\Core\Traits;


trait Filterable
{
    /** @var Array $searchable  */
    /** @var Array $relation_searchable  */

    /** @var Array $filterable  */

    /**
     * Filter a Eloquent Model
     *
     * @param array $filters
     * @param  array $search
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function filter($filters = [], $search = null, $query = null){

        if(!isset($query)){
            $query = static::select('*');
        }

        if(!empty($filters)) {
            foreach ($filters as $filter=>$value){
                if(isset($value) && $value !== "") {

                    if(!empty(static::$filterable[$filter])){
                        $operator = static::$filterable[$filter];
                        switch ($operator){
                            case 'in':
                                $query->whereIn($filter, (is_array($value) ? $value : [$value]));
                                break;
                            case 'between':
                                $query->whereBetween($filter, $value);
                                break;
                            case 'like':
                                $query->where($filter, $operator, "%".$value."%");
                                break;
                            default:
                                if (method_exists(static::class, $operator)){
                                    $query = static::$operator($query, $value);
                                }else{
                                    $query->where($filter, $operator, $value);
                                }
                        }
                    }else{
                        if(is_array($value)){
                            $query->whereIn($filter, $value);
                        }else{
                            $query->where($filter, $value);
                        }
                    }
                }
            }
        }
        if(!empty($search)) {
            $searchable = static::$searchable;

            if (isset(static::$relations_searchable)){
                $relations_searchable = static::$relations_searchable;

                $query->searchIn($search, $searchable, $relations_searchable);
            }else{
                $query->where(function($query) use ($search, $searchable) {
                    foreach ($searchable as $field){
                        $query->orWhere($field, 'LIKE',  '%'.$search.'%');
                    }
                });
            }
        }

        return $query;
    }
}
