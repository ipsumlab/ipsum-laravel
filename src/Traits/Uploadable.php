<?php


namespace Ipsumlab\Core\Traits;

use File;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManagerStatic as Image;

trait Uploadable
{

    protected $files_directory = 'uploads';

    protected $prefix = 'date';
    protected $prefix_format = 'YmdHis';
    protected $uglify_filename = false;
    protected $id_directory = true;
    protected $make_thumb = true;

    public function getAsset($column){
        return asset('storage/'.$this->files_directory. ($this->id_directory ? '/'.$this->id:'').'/'.$this->{$column});
    }
    public function upload(UploadedFile $file, $column = null, $delete_old = false){

        $base_dir = storage_path($this->files_directory);

        $dir = $base_dir . ($this->id_directory ? '/'.$this->id:'');
        $filename = '';

        if(!File::exists($base_dir)){
            File::makeDirectory($base_dir);
        }
        if (!File::exists($dir)) {
            File::makeDirectory($dir);
        }
        switch ($this->prefix){
            case 'date':
                $filename .= date($this->prefix_format) . "_";
                break;
            default:
                $filename .= $this->prefix;
        }

        if($this->uglify_filename){
            $filename .= md5($file->getClientOriginalName());
        }else{
            $filename .= $file->getClientOriginalName();
        }

        $upload_success = $file->move(
            $dir, $filename
        );

        if (!$upload_success) {
            throw new \Exception('Errore durante il caricamento del file ' . $filename . ' in ' . $dir);
        }
        if($this->make_thumb){
            $this->makeThumb($filename);
        }

        if(isset($column)) {
            if($delete_old){
                File::delete($dir."/". $this->{$column});
            }
            $this->{$column} = $filename;
            $this->save();
        }
        return $filename;
    }

    public function makeThumb($filename){

        $base_dir = storage_path($this->files_directory);

        if(!File::exists($base_dir. '/thumbs/')) {
            File::makeDirectory($base_dir. '/thumbs/');
        }


        Image::make($base_dir ."/". $filename)
            ->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->save($base_dir. '/thumbs/'. $filename);
    }
    public function deleteFile($column)
    {
        if(isset($this->id)){
            $dir = storage_path($this->files_directory . '/' . ($this->id_directory ? '/'.$this->id:''));
            File::delete($dir."/thumbs/". $this->{$column});
            return File::delete($dir."/". $this->{$column});
        }
        throw new \Exception('Model not loaded, no valid id provided');
    }

}